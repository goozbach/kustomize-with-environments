.PHONY: int prod stage working/apps/myapp/int working/apps/myapp/prod working/apps/myapp/stage broken

int: working/apps/myapp/int
	@echo -n "# "
	kustomize build $?

stage: working/apps/myapp/stage
	@echo -n "# "
	kustomize build $?

prod: working/apps/myapp/prod
	@echo -n "# "
	kustomize build $?

broken: broken/apps/myapp/int
	@echo -n "# "
	kustomize build $?

