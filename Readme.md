# Kustomize with environments

## Problem
I want to use kustomize to drive my apps configuration via ConfigMaps.
Currently I have a `configMapGenerator` in [working/apps/myapp/GLOBAL/kustomization.yml](working/apps/myapp/GLOBAL/kustomization.yml) named `kustom-configmap`, which expands to `myapp-kustom-configmap`.

That configmap in GLOBAL is merged in each per-env app configuration for
example in
[working/apps/myapp/int/kustomization.yml](working/apps/myapp/int/kustomization.yml).

What I'm wanting to do is have all of the environment-level configMapGenerators
merge down into `kustom-configmap`.

### Current Output
Right now when I run `kustomize build working/apps/myapp/int` I get the following
kubernetes resources output:

```
apiVersion: v1
data:
  COMPANY_NAME: examplecom
kind: ConfigMap
metadata:
  name: global-environment
---
apiVersion: v1
data:
  AWS_REGION: us-west-2
  DB_HOST: int.us-west-2.rds.example.com
  ENVIRONMENT_NAME: int
kind: ConfigMap
metadata:
  name: int-environment
---
apiVersion: v1
data:
  ENV_NAME: int
  MYAPP_NAME: myapp
kind: ConfigMap
metadata:
  name: myapp-kustom-configmap
```


### Wanted Output
What I'm looking for is a single configmap from a merged generator like this:

```
# kustomize build broken/apps/myapp/int
apiVersion: v1
data:
  ENV_NAME: int
  MYAPP_NAME: myapp
  COMPANY_NAME: examplecom
  AWS_REGION: us-west-2
  DB_HOST: int.us-west-2.rds.example.com
  ENVIRONMENT_NAME: int
kind: ConfigMap
metadata:
  name: myapp-kustom-configmap
```
